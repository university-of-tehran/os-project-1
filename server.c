#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h>

int SERVER_FILE_PORT = 8000;
char *SERVER_FILE_PORT_STRING = "8000";
int HEARTBEAT_PORT;
char buf[1024];

void checkArguments(int argc)
{
  if (argc != 2)
  {
    write(1, "Error in number of arguments\n", sizeof("Error in number of arguments\n"));
    exit(0);
  }
}

void sendHeartbeat()
{
  int opt = 1;
  int broadcast = 1;
  int heartbeatSocket;
  struct sockaddr_in heartbeatAddress;
  heartbeatSocket = socket(AF_INET, SOCK_DGRAM, 0);
  setsockopt(heartbeatSocket, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));
  heartbeatAddress.sin_family = AF_INET;
  heartbeatAddress.sin_port = htons(HEARTBEAT_PORT);
  memset(heartbeatAddress.sin_zero, '\0', sizeof heartbeatAddress.sin_zero);
  write(1, ".\n", sizeof(".\n"));
  sendto(heartbeatSocket, SERVER_FILE_PORT_STRING, strlen(SERVER_FILE_PORT_STRING),
         0, (const struct sockaddr *)&heartbeatAddress,
         sizeof(heartbeatAddress));
  close(heartbeatSocket);
  signal(SIGALRM, sendHeartbeat);
  alarm(1);
}

void getFile(int socket)
{
}

void sendFile(int socket)
{
}

int main(int argc, char **argv)
{
  int server_fd, new_socket, valread;
  struct sockaddr_in address;
  int opt = 1;
  int addrlen = sizeof(address);

  write(1, "Hey I'm the server\n", sizeof("Hey I'm the server\n"));
  checkArguments(argc);
  HEARTBEAT_PORT = atoi(argv[1]);
  sendHeartbeat();

  // server_fd = socket(AF_INET, SOCK_STREAM, 0);
  // setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
  // address.sin_family = AF_INET;
  // address.sin_addr.s_addr = INADDR_ANY;
  // address.sin_port = htons(SERVER_FILE_PORT);
  // bind(server_fd, (struct sockaddr *)&address, sizeof(address));
  // listen(server_fd, 3);
  // write(1, "I'm listening\n", sizeof("I'm listening\n"));

  while (1)
  {
    // new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen);
    // write(1, "Got a client\n", sizeof("Got a client\n"));
    // valread = read(new_socket, buf, sizeof(buf));
    // if (strcmp(buf, "down\n") == 0)
    // {
    //   write(1, "it's a download request\n", sizeof("it's a download request\n"));
    //   sendFile(new_socket);
    // }
    // else if (strcmp(buf, "up\n") == 0)
    // {
    //   write(1, "it's an upload request\n", sizeof("it's an upload request\n"));
    //   getFile(new_socket);
    // }
    // else
    // {
    //   write(1, "it's an invalid request\n", sizeof("it's an invalid request\n"));
    //   send(new_socket, "Invalid", strlen("Invalid"), 0);
    // }

    // send(new_socket, "yo man", strlen("yo man"), 0);
    // printf("%s\n", buf);
  }
}