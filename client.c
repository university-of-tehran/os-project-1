#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/time.h>

char buf[1024];
int SERVER_FILE_PORT = 8000;
char *SERVER_FILE_PORT_STRING = "8000";

int checkHeartbeat(int heartbeatPort)
{
  int isServerUp = 0;
  int opt = 1;
  unsigned int len;
  int heartbeatSocket;
  struct sockaddr_in heartbeatAddress;
  struct timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 0;
  heartbeatSocket = socket(PF_INET, SOCK_DGRAM, 0);
  setsockopt(heartbeatSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt));
  setsockopt(heartbeatSocket, SOL_SOCKET, SO_REUSEPORT, (char *)&opt, sizeof(opt));
  setsockopt(heartbeatSocket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
  heartbeatAddress.sin_family = AF_INET;
  heartbeatAddress.sin_addr.s_addr = INADDR_ANY;
  heartbeatAddress.sin_port = htons(heartbeatPort);
  bind(heartbeatSocket, (struct sockaddr *)&heartbeatAddress, sizeof(heartbeatAddress));
  memset(buf, 0, sizeof(buf));

  isServerUp = recvfrom(heartbeatSocket, (char *)buf, sizeof(buf),
                        0, (struct sockaddr *)&heartbeatAddress,
                        &len);
  close(heartbeatSocket);
  if (isServerUp != -1)
  {
    write(1, "Server is up and ready.\n", sizeof("Server is up and ready.\n"));
  }
  else
  {
    write(1, "Server is down, I'll use peer-to-peer connection.\n", sizeof("Server is down, I'll use peer-to-peer connection.\n"));
  }
  return isServerUp;
}

void checkArguments(int argc)
{
  if (argc != 2)
  {
    write(1, "Error in number of arguments\n", sizeof("Error in number of arguments\n"));
    exit(0);
  }
}

void getCommand(char *command)
{
  write(1, "Write your command:\n", sizeof("Write your command:\n"));
  memset(command, 0, sizeof(command));
  read(0, command, sizeof(command));
}

void getFileName(char *filename)
{
  write(1, "Write the file name:\n", sizeof("Write the file name:\n"));
  memset(filename, 0, sizeof(filename));
  read(0, filename, sizeof(filename));
}

void downloadFile(char *filename)
{
  int sock = 0, valread;
  struct sockaddr_in serv_addr;

  sock = socket(AF_INET, SOCK_STREAM, 0);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(SERVER_FILE_PORT);
  inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);
  connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
  send(sock, "hello man", strlen("hello man"), 0);
  valread = read(sock, buf, 1024);
  printf("%s\n", buf);
}

void uploadFile(char *filename)
{
}

int main(int argc, char **argv)
{
  char command[1024];
  char filename[1024];
  int isServerUp = 0;

  checkArguments(argc);

  write(1, "hey I'm a client\n", sizeof("hey I'm a client\n"));

  isServerUp = checkHeartbeat(atoi(argv[1]));

  while (1)
  {
    getCommand(command);
    if (strcmp(command, "down\n") == 0)
    {
      printf("%d\n", 2);
      getFileName(filename);
      downloadFile(filename);
    }
    else if (strcmp(command, "up\n") == 0)
    {
      printf("%d\n", 1);
      getFileName(filename);
      uploadFile(filename);
    }
    else
    {
      printf("%d\n", 3);
      write(1, "This is not a valid command.\n", sizeof("This is not a valid command.\n"));
      continue;
    }
  }
}
